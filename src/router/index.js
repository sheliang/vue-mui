import Vue from 'vue'
import Router from 'vue-router'
//import Hello from '@/components/Hello'
import Home from '@/components/home'
import Index from '@/components/view/index'
import Setting from '@/components/view/setting'
import Login from '@/components/login'
import Reg from '@/components/register'
import Account from '@/components/setting/account'
import notifications from '@/components/setting/notifications'
import notifications_disturb from '@/components/setting/notifications_disturb'
import privacy from '@/components/setting/privacy'
import general from '@/components/setting/general'
import about from '@/components/setting/about'
import feedback from '@/components/setting/feedback'

Vue.use(Router)

export default new Router({
	routes: [{
		path: '/',
		name: '首页',
		component: Home,
		children: [{
			path: '/home',
			name: 'index',
			component: Index
		}]
	}, {
		path: '/setting',
		name: '设置',
		component: Setting
	}, {
		path: '/login',
		name: '登录',
		component: Login
	}, {
		path: '/register',
		name: '注册',
		component: Reg
	}, {
		path: '/account',
		name: '账户与安全',
		component: Account
	}, {
		path: '/notifications',
		name: '新消息通知',
		component: notifications
	}, {
		path: '/notifications_disturb',
		name: '功能消息免扰',
		component: notifications_disturb
	}, {
		path: '/privacy',
		name: '隐私',
		component: privacy
	}, {
		path: '/general',
		name: '通用',
		component: general
	}, {
		path: '/about',
		name: '关于',
		component: about
	}, {
		path: '/feedback',
		name: '反馈',
		component: feedback
	}]
})